#Logger
Java application act as adapter to Sewio WebSocket server and logs tracked data to ELK stack.

[Bitbucket repository](https://bitbucket.org/davidboron/logger/)
  
###Build
```
mvn clean install
```  
  
###Run
From root of the application run:
```
./target/bin/webapp.bat
```
  
*An instance of Embedded Tomcat is created and run on localhost.*
  
  
After start application connects to WebSocket server and starts logging feed data to **/logs/logstash.log** from where it collects logstash.

Second function which the app provides is tracking position of feeds based on their indoor location available on [http://localhost:8080/](http://localhost:8080/)


###Set up mail connection

In order to use mail notifications, you must set up mail account.
Open file 
```
elasticsearch-6.2.3\config\elasticsearch-6.2.3\config\elasticsearch.yml
```

and enter proper email configuration based on this template:
```
xpack.notification.email.account:
    gmail_account:
        profile: gmail
        smtp:
            auth: true
            starttls.enable: true
            host: smtp.gmail.com
            port: 587
            user: <username>
            password: <password>
```
