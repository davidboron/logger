package cz.muni.fi.bozp.monitoring.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author David Boron <373993@mail.muni.cz>
 */
public class Response {

    @SerializedName("body")
    private Feed feed;

    @Override
    public String toString() {
        return "Response{" + "feed=" + feed + '}';
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }
}
