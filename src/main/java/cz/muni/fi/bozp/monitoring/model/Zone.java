package cz.muni.fi.bozp.monitoring.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author David Boron <373993@mail.muni.cz>
 */
public class Zone {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("type")
    private String type;

    @SerializedName("plan_name")
    private String planName;

    @SerializedName("building_reference")
    private String building;

    @SerializedName("at")
    private String datetime;

    @Override
    public String toString() {
        return "Zone{" + "id='" + id + '\'' + ", name='" + name + '\'' + ", type='" + type + '\'' + ", planName='" + planName + '\'' + ", building='" + building + '\'' + ", datetime='" + datetime + '\'' + '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
