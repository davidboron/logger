package cz.muni.fi.bozp.monitoring.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * @author David Boron <373993@mail.muni.cz>
 */
public class Feed {

    @SerializedName("id")
    private String id;

    @SerializedName("datastreams")
    private List<DataStream> dataStreams;

    private List<Zone> zones;

    @Override
    public String toString() {
        return "Feed{" + "id='" + id + '\'' + ", dataStreams=" + dataStreams + ", zones=" + zones + '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<DataStream> getDataStreams() {
        return dataStreams;
    }

    public void setDataStreams(List<DataStream> dataStreams) {
        this.dataStreams = dataStreams;
    }

    public List<Zone> getZones() {
        return zones;
    }

    public void setZones(List<Zone> zones) {
        this.zones = zones;
    }
}
