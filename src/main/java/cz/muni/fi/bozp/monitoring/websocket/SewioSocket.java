package cz.muni.fi.bozp.monitoring.websocket;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;
import cz.muni.fi.bozp.monitoring.model.DataStream;
import cz.muni.fi.bozp.monitoring.model.Feed;
import cz.muni.fi.bozp.monitoring.model.Response;
import cz.muni.fi.bozp.monitoring.model.Zone;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.logstash.logback.argument.StructuredArguments.*;

/**
 * Custom WebSocket Sewio client
 *
 * @author David Boron <373993@mail.muni.cz>
 */
@WebSocket(maxTextMessageSize = 64 * 1024)
public class SewioSocket {

    private Session session;
    private String apiKey;
    private Map<String, Feed> currentFeeds;
    private Logger logger = LoggerFactory.getLogger(SewioSocket.class);
    private Logger logstashLogger = LoggerFactory.getLogger("cz.muni.fi.bozp.monitoring.websocket.SewioSocket:logstash");

    public static final String isInInfoZone = "isInInfoZone";
    public static final String isInValidZone = "isInValidZone";
    public static final String isInWarningZone = "isInWarningZone";
    public static final String isInDangerZone = "isInDangerZone";

    /**
     * Constructor
     *
     * @param apiKey
     */
    public SewioSocket(String apiKey) {
        logger.info("Creating Sewio socket");
        this.apiKey = apiKey;
        this.currentFeeds = new HashMap<>();
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        logger.info("Connection successful");
        this.session = session;
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        logger.info("Connection closed: {} - {}", statusCode, reason);
        this.session = null;
    }

    /**
     * Send message to WS server
     *
     * @param msg
     */
    public void sendMessage(String msg) {
        try {
            Future<Void> fut;
            fut = session.getRemote().sendStringByFuture(msg);
            fut.get(2, TimeUnit.SECONDS);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * Subscribe for data fetching
     *
     * @param FEED_ID id of Feed to fetch
     */
    public void subscribe(String FEED_ID) {
        String method = "subscribe";
        String msg = "{\"headers\":{\"X-ApiKey\":\"" + this.apiKey + "\"},\"method\":\"" + method + "\", \"resource\":\"/feeds/" + FEED_ID + "\"}";
        logger.info("Subscribe to feed: {}", FEED_ID);
        logger.debug("Sending msg: {}", msg);
        sendMessage(msg);
    }

    /**
     * Stop fetching data
     *
     * @param FEED_ID id of Feed to unsubscribe
     */
    public void unsubscribe(String FEED_ID) {
        String method = "unsubscribe";
        String msg = "{\"headers\":{\"X-ApiKey\":\"" + this.apiKey + "\"},\"method\":\"" + method + "\", \"resource\":\"/feeds/" + FEED_ID + "\"}";
        logger.info("Sending msg: {}", msg);
        sendMessage(msg);
    }

    /**
     * Send coordinates to WS server (CURRENTLY NOT IN USE)
     *
     * @param FEED_ID
     * @param posX
     * @param posY
     */
    public void put(String FEED_ID, double posX, double posY) {
        String method = "put";
        String msg = "{\"headers\":{\"X-ApiKey\":\"" + this.apiKey + "\"},\"method\":\"" + method + "\",options:{savetoDB:false}, \"body\":{\"id\":" + FEED_ID + ",\"datastreams\":[{\"id\":\"posX\"," +
            "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "" + "\"current_value\":" + posX + "},{\"id\":\"posY\",\"current_value\":" + posY + "}]}, \"resource\":\"/feeds/" + FEED_ID + "\"}";
        logger.debug("Sending msg: {}", msg);
        sendMessage(msg);
    }

    /**
     * Check if client is connected
     *
     * @return
     */
    public boolean isConnected() {
        if (session != null) {
            return session.isOpen();
        } else {
            return false;
        }
    }

    /**
     * Message callback
     * processing data
     *
     * @param msg
     */
    @OnWebSocketMessage
    public void onMessage(String msg) {
        Gson gson = new Gson();
        Response response = gson.fromJson(msg, Response.class);
        Feed feed = response.getFeed();

        // convert ArrayList of DataStream into HashMap
        Map dataStreamsMap = this.convertFeedListToMap(feed.getDataStreams());

        int isMoving = isMoving(feed.getId(), dataStreamsMap) ? 1 : 0;
        boolean isInZone = feed.getZones() != null;

        Map zonesMap = createZoneMap();

        if (isInZone) {
            for (Zone zone : feed.getZones()) {
                switch (zone.getType()) {
                    case "info":
                        zonesMap.put(isInInfoZone, 1);
                        break;
                    case "valid":
                        zonesMap.put(isInValidZone, 1);
                        break;
                    case "warning":
                        zonesMap.put(isInWarningZone, 1);
                        break;
                    case "danger":
                        zonesMap.put(isInDangerZone, 1);
                        break;
                }
            }
        }

        //        main feed log
        logstashLogger.info(
            "Feed log [id:{}]",
            feed.getId(),
            keyValue("id", feed.getId()),
            entries(dataStreamsMap),
            entries(zonesMap),
            keyValue("isMoving", isMoving));

        updateFeed(feed);
    }

    /**
     * Convert List of mixed data streams to map
     *
     * @param dataStreams list of data streams
     * @return converted Map
     */
    private Map convertFeedListToMap(List<DataStream> dataStreams) {
        Map<String, Object> dataStreamsMap = new HashMap<>();
        for (DataStream dataStream : dataStreams) {
            dataStreamsMap.put(dataStream.getId(), dataStream.getValue());
            switch (dataStream.getId()) {
                case "posX":
                case "posY":
                case "posZ":
                case "pressure":
                case "temperature":
                    dataStreamsMap.put(dataStream.getId(), Float.parseFloat(dataStream.getValue()));
                    break;
                case "batLevel":
                    dataStreamsMap.put(dataStream.getId(), Float.parseFloat(dataStream.getValue().replace("%", "")));
                    break;
                default:
                    dataStreamsMap.put(dataStream.getId(), dataStream.getValue());
            }
        }
        return dataStreamsMap;
    }

    /**
     * Save latest data feed locally
     *
     * @param feed
     */
    private void updateFeed(Feed feed) {
        currentFeeds.put(feed.getId(), feed);
    }

    /**
     * Check if tag is moving based on difference between last known position
     *
     * @param id of feed
     * @param dataStreams of feed
     * @return boolean value representing whether is moving
     */
    private boolean isMoving(String id, Map dataStreams) {

        if (!currentFeeds.containsKey(id)) {
            return true;
        }

        Map previousDataStreams = this.convertFeedListToMap(currentFeeds.get(id).getDataStreams());

        boolean result = dataStreams.get("posX").equals(previousDataStreams.get("posX")) && dataStreams.get("posY").equals(previousDataStreams.get("posY")) && dataStreams.get("posZ").equals
            (previousDataStreams.get("posZ"));

        return !result;
    }

    /**
     * Prepare empty map with every supported zone
     * @return
     */
    private Map createZoneMap() {
        Map zonesMap = new HashMap<String, Integer>();
        zonesMap.put(isInInfoZone, 0);
        zonesMap.put(isInValidZone, 0);
        zonesMap.put(isInWarningZone, 0);
        zonesMap.put(isInDangerZone, 0);
        return zonesMap;
    }

}
