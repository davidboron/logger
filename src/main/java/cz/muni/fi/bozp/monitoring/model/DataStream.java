package cz.muni.fi.bozp.monitoring.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author David Boron <373993@mail.muni.cz>
 */
public class DataStream {

    @SerializedName("id")
    private String id;

    @SerializedName("current_value")
    private String value;

    @SerializedName("at")
    private String datetime;

    @Override
    public String toString() {
        return "DataStream{" + "id='" + id + '\'' + ", value='" + value + '\'' + ", datetime='" + datetime + '\'' + '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
