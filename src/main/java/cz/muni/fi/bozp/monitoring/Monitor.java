package cz.muni.fi.bozp.monitoring;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.ArrayMap;
import com.google.gson.reflect.TypeToken;
import cz.muni.fi.bozp.monitoring.websocket.SewioSocket;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static launch.Main.loadConfig;

/**
 * Monitoring class
 *
 * @author David Boron <373993@mail.muni.cz>
 */
public class Monitor {

    private static final int MAX_CONNECTION_REPEATS = 5;
    private static HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static JsonFactory GSON_FACTORY = new GsonFactory();
    private static Logger logger = LoggerFactory.getLogger(Monitor.class);

    /**
     * Create WS client and start continual monitoring
     */
    public static void start() {

        logger.info("Starting application...");

        Properties config;
        try {
            config = loadConfig();
        } catch (IOException e) {
            throw new IllegalStateException("Config cannot be loaded", e);
        }
        final String server = config.getProperty("server");
        final String port = config.getProperty("port");
        final String apiKey = config.getProperty("apiKey");

        String allFeedsPath = "http://" + config.getProperty("server") + "/sensmapserver/api/feeds";
        Map allFeeds = fetchAllFeeds(allFeedsPath);
        List tags = filterTags((ArrayList) allFeeds.get("results"));

        WebSocketClient client = new WebSocketClient();
        SewioSocket socket = new SewioSocket(apiKey);

        try {
            client.start();

            URI rtlsUri = new URI("ws://" + server + ":" + port);
            client.connect(socket, rtlsUri);
            logger.info("Connecting to : {}", rtlsUri);

            int i = 0;
            while (!socket.isConnected()) {
                if (i == MAX_CONNECTION_REPEATS) {
                    logger.info("Connection failed, closing websocket.");
                    return;
                }

                Thread.sleep(1000);
                logger.info("Unable to connect. Trying again.");
                i++;
            }

            // subscribe to every tag
            tags.forEach(item -> {
                String id = (String) ((ArrayMap) item).get("id");
                socket.subscribe(id);
            });

            logger.info("Starting monitoring");
            while (socket.isConnected()) {
            }

        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            try {
                client.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Get all feeds for fetching from REST API
     *
     * @param path Url
     * @return JSON data (custom structure)
     */
    private static GenericJson fetchAllFeeds(String path) {

        logger.info("Fetching feeds...");

        HttpRequestFactory requestFactory = HTTP_TRANSPORT.createRequestFactory((HttpRequest request) -> {
            request.setParser(new JsonObjectParser(GSON_FACTORY));
        });

        GenericUrl url = new GenericUrl(path);
        HttpRequest request = null;
        try {
            request = requestFactory.buildGetRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Type type = new TypeToken<GenericJson>() {
        }.getType();
        GenericJson data = null;
        try {
            data = (GenericJson) request.execute().parseAs(type);
        } catch (IOException e) {
            logger.warn("Parsing failed", e);
        }

        return data;
    }

    /**
     * Filter tags from list
     *
     * @param feeds List of mixed items
     * @return List of tags
     */
    private static List filterTags(ArrayList feeds) {

        logger.info("Filtering tags...");

        List tags = new ArrayList();

        if (feeds.size() == 0) {
            throw new IllegalStateException("Feeds array is empty");
        }

        for (int i = 0; i < feeds.size(); i++) {
            ArrayMap item = (ArrayMap) feeds.get(i);
            if (item.get("type").equals("tag")) {
                tags.add(item);
            }
        }

        return tags;
    }

}
