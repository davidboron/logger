package launch;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.servlet.ServletException;

import cz.muni.fi.bozp.monitoring.Monitor;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Launcher class, starts app
 *
 * @author David Boron <373993@mail.muni.cz>
 */
public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    private static final String CONFIG_FILENAME = System.getProperty("env") != null ? "config." + System.getProperty("env") + ".properties" : "config.properties";
    private static final String PORT = "8080";

    /**
     * Main method starts embedded Tomcat and monitoring loop
     *
     * @param args
     */
    public static void main(String[] args) {


        try {
            Properties config = loadConfig();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Start monitoring
        new Thread(new Runnable() {
            public void run() {
                Monitor.start();
            }
        }).start();

        // Start embedded Tomcat
        startServer();
    }

    /**
     * Embedded tomcat impl
     * starts server
     */
    private static void startServer() {

        String webappDirLocation = "src/main/webapp/";
        Tomcat tomcat = new Tomcat();

        String webPort = System.getProperty("PORT");
        if (webPort == null || webPort.isEmpty()) {
            webPort = PORT;
        }

        tomcat.setPort(Integer.valueOf(webPort));

        StandardContext ctx = null;
        try {
            ctx = (StandardContext) tomcat.addWebapp("", new File(webappDirLocation).getAbsolutePath());
        } catch (ServletException e) {
            logger.warn("Cannot create Tomcat context", e);
            return;
        }
        logger.info("configuring app with basedir: " + new File("./" + webappDirLocation).getAbsolutePath());

        File additionWebInfClasses = new File("target/classes");
        WebResourceRoot resources = new StandardRoot(ctx);
        resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes", additionWebInfClasses.getAbsolutePath(), "/"));
        ctx.setResources(resources);

        try {
            tomcat.start();
        } catch (LifecycleException e) {
            logger.warn("Cannot start Tomcat server", e);
            return;
        }
        tomcat.getServer().await();
    }

    /**
     * Load properties file as config
     *
     * @return Properties config
     * @throws IOException when file is not found
     */
    public static Properties loadConfig() throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties properties = new Properties();
        try (InputStream resourceStream = loader.getResourceAsStream("config/" + CONFIG_FILENAME)) {
            properties.load(resourceStream);
        }

        return properties;
    }
}
