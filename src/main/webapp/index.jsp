<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8>
    <title>Monitor</title>
    <link href="css/main.css" rel="stylesheet">
</head>
<body>
<div id=root></div>
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>